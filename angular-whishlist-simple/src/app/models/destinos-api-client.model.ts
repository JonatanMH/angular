import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { DestinoViaje } from './destino-viaje.models';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.models';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse} from  '@angular/common/http'

@Injectable()
export class DestinosApiClient {
	destinos: DestinoViaje[] = [];
	constructor(private store: Store<AppState>,
		@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
		private http: HttpClient) {
     
	}

	add(d: DestinoViaje) {
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
		  if (data.status === 200) {
			this.store.dispatch(new NuevoDestinoAction(d));	
			const myDb = db;
			myDb.destinos.add(d);
			console.log('todos los destinos de la db!');
			myDb.destinos.toArray().then(destinos => console.log(destinos))		
		  }
		});
	  }
	

	  getAll(): DestinoViaje[] {
		return this.destinos;
	  }
	

	elegir (d:DestinoViaje) {
		this.store.dispatch(new ElegidoFavoritoAction(d))
		
		
	}


	getById(Id:string):DestinoViaje{
		console.log('Llamado por la clase decorada');
		console.log('config');
		return null;
	}
}